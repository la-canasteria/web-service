﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CADCargarVentas
/// </summary>
public class CADCargarVentas
{
    private MySqlConnection conexion;

    public CENResponseVentas CargarVentas(listaVentas lista, string ip)
    {
        string xmlproducto = null;
        CENResponseVentas responseVentas = new CENResponseVentas();
        MySqlDataReader dr = null;
        CADConexion conex = new CADConexion();

        conexion = new MySqlConnection(conex.CxSQL());
        conex.OpenConection(conexion);

        List<ventas> listado = lista.ventas;
        try
        {
        
           responseVentas.Respuesta = 0;
           responseVentas.Descripcion = "";
           foreach (ventas datos in listado)
           {
               if (datos.CodigoPedido > 0)
                   
                {
                   using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_cargarventas", conexion))
                   {
                        List<ListaDetalleVentas> detalle = datos.ListaDetalleVentas;
                        xmlproducto = "";
                        foreach (ListaDetalleVentas producto in detalle) {
                            xmlproducto = xmlproducto + "<detalleproducto>";
                            xmlproducto = xmlproducto + "<item>"+producto.item+"</item>";
                            xmlproducto = xmlproducto + "<articulo>" + producto.CodigoArticulo + "</articulo>";
                            xmlproducto = xmlproducto + "<descripcion>" + producto.DescripcionArticulo + "</descripcion>";
                            xmlproducto = xmlproducto + "<categoria>" + producto.CodigoCategoria + "</categoria>";
                            xmlproducto = xmlproducto + "<cantventa>" + producto.CantidadVenta + "</cantventa>";
                            xmlproducto = xmlproducto + "<impventa>" + producto.ImporteVenta + "</impventa>";
                            xmlproducto = xmlproducto + "<descuento>" + producto.CodigoDescuento + "</descuento>";
                            xmlproducto = xmlproducto + "<impdescuento>" + producto.ImporteDescuento + "</impdescuento>";
                            xmlproducto = xmlproducto + "</detalleproducto>";
                        }
                        DateTime fech = DateTime.Parse(datos.FechaPedido);
                        datos.FechaPedido = fech.ToString("yyyy-MM-dd");
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new MySqlParameter("p_codped", MySqlDbType.Int64)).Value = datos.CodigoPedido;
                        command.Parameters.Add(new MySqlParameter("p_fechped", MySqlDbType.VarChar)).Value = datos.FechaPedido;
                        command.Parameters.Add(new MySqlParameter("p_impped", MySqlDbType.Decimal)).Value = datos.ImportePedido;
                        command.Parameters.Add(new MySqlParameter("p_codbin", MySqlDbType.VarChar)).Value = datos.CodigoBIN;
                        command.Parameters.Add(new MySqlParameter("p_xmlpro", MySqlDbType.Text)).Value = xmlproducto;
                        command.Parameters.Add(new MySqlParameter("p_ip", MySqlDbType.VarChar)).Value = ip;
                        command.CommandTimeout = 0;
                       dr = command.ExecuteReader();
                       if (dr.HasRows)
                       {
                           while (dr.Read())
                           {
                                responseVentas.Respuesta = Convert.ToInt32(dr["codigo"].ToString());
                                responseVentas.Descripcion = dr["mensaje"].ToString();
                           }
                       }
                        if (dr != null)
                        {
                            dr.Close();
                        }
                    }
               }
           }
            return responseVentas;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally

        {
            conex.CerrarConexion(conexion);
            if (dr != null)
            {
                dr.Close();
            }
            
        }
    }
}