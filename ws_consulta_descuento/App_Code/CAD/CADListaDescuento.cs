﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CADListaDescuento
/// </summary>
public class CADListaDescuento
{
    private MySqlConnection conexion;
    public int item;
    public string categoria;
    public int tipodescuento;
    public string banco;
    public string tipotarjeta;
    public string marca;
    public decimal pordescuento;
    public int codigo;
    public string descripcion;
    public CENListaDescuento CADListDescuento(string Fecha, string hora, string codbin, string xmlDetalle)
    {

        CENListaDescuento CENListaDescuento = new CENListaDescuento();
        MySqlDataReader dr;
        CADConexion conex = new CADConexion();
   


        try
        {
            DateTime fech = DateTime.Parse(Fecha);
            Fecha = fech.ToString("yyyy-MM-dd");
            string xml_result = "";


            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_consultar_descuentos", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("p_fecha", MySqlDbType.String)).Value = Fecha;
                    command.Parameters.Add(new MySqlParameter("p_hora", MySqlDbType.VarChar)).Value = hora;
                    command.Parameters.Add(new MySqlParameter("p_cbin", MySqlDbType.VarChar)).Value = codbin;
                    command.Parameters.Add(new MySqlParameter("p_xml_deta", MySqlDbType.VarChar)).Value = xmlDetalle;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    item = 0;
                    categoria = "";
                    tipodescuento = 0;
                    banco = "";
                    tipotarjeta = "";
                    marca = "";
                    pordescuento = 0;
                    codigo = 0;
                    descripcion = "";
                    if (dr.HasRows)
                    {
                        xml_result = "<listaDescuentos>";
                        while (dr.Read())
                        {
                            //CENListaDescuento.item = Convert.ToInt32(dr["l_item"].ToString());
                            //CENListaDescuento.categoria = dr["l_desc_codc"].ToString();
                            //CENListaDescuento.tipodescuento = Convert.ToInt32(dr["l_tdes"].ToString());
                            //CENListaDescuento.banco = dr["l_desc_codb"].ToString();
                            //CENListaDescuento.tipotarjeta = dr["l_desc_ttar"].ToString();
                            //CENListaDescuento.marca = dr["l_desc_codm"].ToString();
                            //CENListaDescuento.pordescuento = Convert.ToDecimal(dr["l_porc"].ToString());
                            //CENListaDescuento.codigo = Convert.ToInt32(dr["l_cod_rpta"].ToString());
                            //CENListaDescuento.descripcion = dr["l_desc_rpta"].ToString();

                            item            = Convert.ToInt32(dr["l_item"].ToString());
                            categoria       = dr["l_desc_codc"].ToString();
                            tipodescuento   =  Convert.ToInt32(dr["l_tdes"].ToString());
                            banco           = dr["l_desc_codb"].ToString();
                            tipotarjeta     = dr["l_desc_ttar"].ToString();
                            marca           = dr["l_desc_codm"].ToString();
                            pordescuento    = Convert.ToDecimal(dr["l_porc"].ToString());
                            codigo          = Convert.ToInt32(dr["l_cod_rpta"].ToString());
                            descripcion     = dr["l_desc_rpta"].ToString();

                           
                            xml_result = xml_result + "<producto>";
                            xml_result = xml_result + "<item>" + item + "</item>";
                            xml_result = xml_result + "<categoria>" + categoria + "</categoria>";
                            xml_result = xml_result + "<tipoDescuento>" + tipodescuento + "</tipoDescuento>";
                            xml_result = xml_result + "<banco>" + banco + "</banco>";
                            xml_result = xml_result + "<tipoTarjeta>" + tipotarjeta + "</tipoTarjeta>";
                            xml_result = xml_result + "<marca>" + marca + "</marca>";
                            xml_result = xml_result + "<porcentajeDescuento>" + pordescuento + "</porcentajeDescuento>";
                            xml_result = xml_result + "</producto>";
                            

                            
                        }
                        xml_result = xml_result + "</listaDescuentos>";
                        CENListaDescuento.xml = xml_result;
                        CENListaDescuento.codigo = codigo;
                        CENListaDescuento.descripcion = descripcion;
                    }
                }

            }
            return CENListaDescuento;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conex.CerrarConexion(conexion);
        }
    }
}