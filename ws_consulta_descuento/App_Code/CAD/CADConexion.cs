﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

/// <summary>
/// Descripción breve de CADConexion
/// </summary>
public class CADConexion
{
    public String CxSQL()
    {
        //DESCRIPCION : CONEXION CON MySQL SERVER
        try
        {
            return ConfigurationManager.ConnectionStrings["MySqlConexion"].ConnectionString.ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void OpenConection(MySqlConnection Connection)
    {
        //DESCRIPCION: INICIA CONEXION CON DB SQL
        try
        {
            if (Connection.State == ConnectionState.Closed) Connection.Open();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void CerrarConexion(MySqlConnection Connection)
    {
        //DESCRIPCION: CIERRA CONEXION CON DB SQL
        try
        {
            if (Connection.State == ConnectionState.Open) Connection.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}