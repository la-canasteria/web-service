﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CENCargarVentas
/// </summary>
public class ventas
{

    public int CodigoPedido { get; set; }
    public string FechaPedido { get; set; }
    public decimal ImportePedido { get; set; }
    public string CodigoBIN { get; set; }
    public List<ListaDetalleVentas> ListaDetalleVentas { get; set; }
    

  

}


public class ListaDetalleVentas
{
    public int item { get; set; }
    public string CodigoArticulo { get; set; }
    public string DescripcionArticulo { get; set; }
    public int CodigoCategoria { get; set; }
    public int CantidadVenta { get; set; }
    public decimal ImporteVenta { get; set; }
    public int CodigoDescuento { get; set; }
    public decimal ImporteDescuento { get; set; }
}


public class listaVentas
{
    public List<ventas> ventas { get; set; }
    public CENErrWs ErrorWebSer { get; set; }
}
