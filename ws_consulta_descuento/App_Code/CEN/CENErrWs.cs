﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CENErrWs
/// </summary>
public class CENErrWs
{
    //private int tipoErr;// tipo de error 
    //private int codigoErr; // codigo de error 
    //private string descripcionErr; // descripcion de error 

    public int TipoErr { get; set; }
    public int CodigoErr { get; set; }
    public string DescripcionErr { get; set; }
}