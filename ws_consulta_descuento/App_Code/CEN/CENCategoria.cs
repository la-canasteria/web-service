﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Descripción breve de CENCategoria
/// </summary>
public class CENCategoria
{

    public int id;
    public string name;
    public string slung;
    public int parent;
    public string descripcion;
    public string display;
    public object image;
    public int menu_order;
    public int count;
    public object links;

    public int Id { get => id; set => id = value; }
    public string Name { get => name; set => name = value; }
    public string Slung { get => slung; set => slung = value; }
    public int Parent { get => parent; set => parent = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }
    public string Display { get => display; set => display = value; }
    public object Image { get => image; set => image = value; }
    public int Menu_order { get => menu_order; set => menu_order = value; }
    public int Count { get => count; set => count = value; }
    public object Links { get => links; set => links = value; }

    
    public CENCategoria()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    
}

public class listaDatos
{
    public List<CENCategoria> listDatos;
    }


