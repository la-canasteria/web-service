﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CLNListaDescuento
/// </summary>
public class CLNListaDescuento
{
    public CENListaDescuento CLNListDescuento(string Fecha, string hora, string codbin, string xmlDetalle)
    {
        CADListaDescuento CADListaDescuento = new CADListaDescuento();
        CENListaDescuento CENListaDescuento = new CENListaDescuento();

        try
        {
            CENListaDescuento = CADListaDescuento.CADListDescuento(Fecha,hora,codbin,xmlDetalle);

            return CENListaDescuento;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}