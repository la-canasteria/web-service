﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CLNCargarVentas
/// </summary>
public class CLNCargarVentas
{
    public CENResponseVentas CargarVentas(listaVentas lista, string ip)
    {
        CADCargarVentas CACargarVentas = new CADCargarVentas();
        CENResponseVentas responseVentas = new CENResponseVentas();

        try
        {
            responseVentas = CACargarVentas.CargarVentas(lista, ip);

            return responseVentas;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}